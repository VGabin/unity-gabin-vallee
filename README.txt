FapLeWazo.zip

Ce qui a été implanté :

 - Navigation entre deux scènes (une de jeu, une d'accueil)
 - Musique qui continue de ce jouer entre les deux écrans
 - Comptage de points avec affichage en sprite
 - Partage sur les réseaux sociaux
 - Apparition d'ennemis aléatoire
 - Différents sound effect
 - Quelques animations (j'en reparles dans l'autre partie)
 - Collison avec les tuyaux/ennmis
 - Gestion du haut et bas de l'écran (quand on va en haut de l'écran, le jeu nous téléporte en bas)

 - On gagne 1 point en passant à travers un tuyau
 - 5 en battant le boss

 - Les ennemies "classiques" peuvent être détruit pendant la phase de boss mais pas les boules de feu tirées par le boss

 - Taux d'apparitions :
	+ Bill ball = 24%
	+ Koopa Rouge = 14%
	+ Koopa Bleues = 14%
	+ Boss = 5%

Ce qui a été essayé mais non réussi :

 - L'ajout d'animation quand je boss attaque. J'ai essayé de regarder divers tutoriels sur internet mais je n'arrive pas à jouer une animation différente lors de l'attaque, quand j'essayais d'ajouter une animation j'avais un message d'erreur qui me disait que l'animation ne pouvait pas être lu (alors que si j'essayais de la mettre en continue elle fonctionnait)

Répartition des rôles : 

 - Tout, tout seul (Gabin Vallée)

Les devices sur lequel il a était testé :

 - Xiaomi 11T pro
 - Redmi note 9
 - Dell G3 (PC)
 - Redmi Note 7 (il fonctionne presque mieux sur ce device)

Point important à préciser :

 - Je n'ai pas eu le temps de sauvegarder le score : je travaillais sur une vieille version de Unity et un vieux SDK mais quand j'ai du faire le projet Flutter j'ai dû tout reinstaller et j'ai perdu énormément de temps
 - Si pour compiler ils demdandent un mot de passe, essayez "12345678"
 - Vous pouvez l'essayez sur PC avec la touche espace