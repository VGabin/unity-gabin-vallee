using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class backgroundsMovements : MonoBehaviour
{
    // Récupère la taille tu background pour déterminer sa sortie d'écran
    private Vector2 backgroundSize;

    void Update()
    {
        // Applique sa vitesse au background pour qu'il puisse bouger
        GetComponent<Rigidbody2D>().velocity = gameManager.backgroundSpeed;

        // Récupère la largeur du background
        backgroundSize.x = gameObject.GetComponent<SpriteRenderer>().bounds.size.x;

        // Si le background sort de l'écran, le tp à une autre postion en dehors de l'écran de l'autre côté afin de boucler 
        if(transform.position.x < gameManager.leftCameraBorder.x - (backgroundSize.x/2))
        {
            transform.position = new Vector3(gameManager.positionRestartX, transform.position.y, transform.position.z);
        }
    }
}
