using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemyManager : MonoBehaviour
{
    // Initie la vitesse
    public Vector2 speed;

    void Start()
    {
        gameObject.transform.parent = GameObject.Find("06 - Enemies").transform;
        // La koopa bleue à un pattern différent, elle saute un peu comme l'wazo
        if (gameObject.name == "blueFlyingKoopa(Clone)")
        {
            InvokeRepeating("BlueKoopa", 0f, 0.8f);
        }
    }

    void Update()
    {
        // Le bill fonce en ligne droite
        if (gameObject.name == "bill(Clone)")
        {
            gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(speed.x -2f, speed.y);
        }
        // La koopa rouge suit le joueur
        else if (gameObject.name == "redFlyingKoopa(Clone)")
        {
            gameObject.GetComponent<Rigidbody2D>().velocity = speed;
            Vector3 targetPosition = new Vector3(gameManager.player.transform.position.x, gameManager.player.transform.position.y, gameObject.transform.position.z); // Définit la position cible de "gameObject" sur celle de "wazo" (même axe Z)
            gameObject.transform.position = Vector3.MoveTowards(gameObject.transform.position, targetPosition, -(speed.x/4) * Time.deltaTime);
        }
        // Le feu aussi
        else if (gameObject.name == "fire(Clone)")
        {
            gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(gameManager.acceleration, 0);
            Vector3 targetPosition = new Vector3(gameManager.player.transform.position.x, gameManager.player.transform.position.y, gameObject.transform.position.z); // Définit la position cible de "attackCloneItem" sur celle de "wazo" (même axe Z)
            gameObject.transform.position = Vector3.MoveTowards(gameObject.transform.position, targetPosition, -(gameManager.acceleration/3) * Time.deltaTime);
        }

        // Détruit l'objet si il quitte l'acran
        if (gameObject.transform.position.x < gameManager.leftCameraBorder.x - (gameObject.GetComponent<SpriteRenderer>().bounds.size.x / 2))
        {
            Destroy(gameObject);
        }
    }

    void BlueKoopa()
    {
        gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(speed.x + gameManager.acceleration -2f, speed.y + 4f);
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.tag == "birdAttack" && gameObject.tag != "bossAttack")
        {
            Destroy(gameObject);
        }
    }
    
}
