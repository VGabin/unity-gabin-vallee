using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bossManager : MonoBehaviour
{
    // Récupère la musique afin de changer à l'apparition du boss
    private music musicScript;

    // Initie les variables pour les gameObjects d'attaque
    private GameObject attack;
    private GameObject attackClone;
    private float attackInterval = 1f;

    // Initie les variables de statistiques du boss
    private GameObject bossSprite;
    private Vector2 bossSpeed;
    private int bossHP;
    private Vector2 bossSize;

    // Indicateur de dégâts subis
    private float colorDecrementation;
    private Color cl;

    void Start()
    {
        // Charge le script de la music et la change pour passer sur le theme du boss
        musicScript = music.instance;
        musicScript.ChangeMusic();
        
        // Charge le gameObject de l'attaque et créer un interval entre chaque apparition
        attack = Resources.Load("gameObjects/fire") as GameObject;
        InvokeRepeating("SpawnObject", 0f, attackInterval);

        // Gère les paramètre du boss
        bossSprite = gameObject.transform.GetChild(0).gameObject;
        bossSpeed = new Vector2(0, 2);
        bossHP = gameManager.score * 8;
        bossSize.y = gameObject.GetComponent<SpriteRenderer>().bounds.size.y;

        // L'indicateur est dynamique : cela permet à l'objet de rougir de plus en plus en fonction de ses points de vie
        colorDecrementation = (1f/bossHP)*gameManager.attackDamage;
    }

    void Update()
    {
        // Si le boss est vivant
        if (bossHP > 0)
        {
            // Bouge de haut en bas
            gameObject.GetComponent<Rigidbody2D>().velocity = bossSpeed;
            if (gameObject.transform.position.y < gameManager.downCameraBorder.y + (bossSize.y/2))
            {
                bossSpeed = new Vector2(0, 2);
            }
            else if (gameObject.transform.position.y > gameManager.upCameraBorder.y - (bossSize.y/2))
            {
                bossSpeed = new Vector2(0, -2);
            }
        }
        else
        {

            // Désactive l'attaque du joueur
            gameManager.canAttack = false;

            foreach (GameObject birdAttack in GameObject.FindGameObjectsWithTag("birdAttack"))
            {
                Destroy(birdAttack);
            }

            // Change le sprite et fait tomber le boss
            gameObject.GetComponent<SpriteRenderer>().enabled = true;
            bossSprite.GetComponent<SpriteRenderer>().enabled = false;
            gameObject.transform.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Dynamic;

            // Le détruit une fois sorti
            if (gameObject.transform.position.y < gameManager.downCameraBorder.y)
            {
                // Ajoute 10 points au score
                gameManager.score += 5;
                    
                // Change la musique
                musicScript.ChangeMusic();
                
                Destroy(gameObject);
            }
        }
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.tag == "birdAttack")
        {
            // Retire les HP
            bossHP -= gameManager.attackDamage;

            // Récupère la couler et la change
            Color cl = bossSprite.GetComponent<SpriteRenderer>().color;
            cl.b -= colorDecrementation;
            cl.g -= colorDecrementation;
            foreach (SpriteRenderer child in gameObject.GetComponentsInChildren<SpriteRenderer>())
            {
                child.color = cl;
            }
        }
    }

    void SpawnObject()
    {
        // Fait attaquer le boss
        if (gameManager.canAttack == true)
        {
            GameObject.FindGameObjectWithTag("gameManager").GetComponent<gameManager>().generateEnemy();
            attackClone = Instantiate(attack, gameObject.transform.position, transform.rotation);
        }     
    }
}
