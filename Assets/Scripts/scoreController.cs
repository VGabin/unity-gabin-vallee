using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class scoreController : MonoBehaviour
{
    public int score = 0;
    public Sprite[] numberSprites; // Initie le tableaux de sprites des chiffres
    public Image[] scoreImages; // Initie le tableaux des images des chiffres pour le score

    // Sert à mettre à jour le score
    public void UpdateScore(int newScore)
    {
        score = newScore;
        int[] digits = GetDigits(score); // Récupère chaque chiffre du score

        // Mets à jour les images de chaque chiffre dans le tableau
        for (int i = 0; i < scoreImages.Length; i++)
        {
            if (i < digits.Length)
            {
                scoreImages[i].sprite = numberSprites[digits[i]];
                scoreImages[i].gameObject.SetActive(true);
            }
            else
            {
                scoreImages[i].gameObject.SetActive(false);
            }
        }
    }

    // Divise le score en chiffres individuels
    int[] GetDigits(int number)
    {
        List<int> digits = new List<int>();
        while (number > 0)
        {
            digits.Add(number % 10);
            number /= 10;
        }
        digits.Reverse(); // Inverse la liste pour obtenir les chiffres dans le bon ordre
        return digits.ToArray();
    }
}