using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class touchAction : MonoBehaviour
{
    // Initie la vitesse, la hitbox et l'audio de l'wazo
    public Vector2 speed;
    Rigidbody2D birdBody;
    AudioSource fap;

    void Start()
    {
        // Leurs donnes des valeurs
        birdBody = gameObject.GetComponent<Rigidbody2D>();
        fap = GetComponent<AudioSource>();
        speed = new Vector2(speed.x, speed.y + 5.25f);
    }

    void Update()
    {
        // Donne une gravité à l'wazo
        birdBody.gravityScale = 1;

        // Grâce à ça je peux test sur mobile et PC
        if (Input.touchCount > 0)
        {
            Touch p = Input.GetTouch(0);
            if (p.phase == TouchPhase.Ended)
            {
                birdBody.velocity = speed;
                fap.Play();
            }
        }
        else if (Input.GetKey("space"))
        {
            birdBody.velocity =speed;
            fap.Play();
        }
    }
    // void OnTriggerEnter2D(Collider2D collider)
    // {
    //     if (collider.tag == "wazo")
    //     {
    //         Debug.Log(collider.tag);
    //     }
    // }
}
