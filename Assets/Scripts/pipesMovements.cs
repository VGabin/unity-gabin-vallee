using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pipesMovements : MonoBehaviour
{
    // Initie les game objects
    public GameObject pipeUp;
    public GameObject pipeDown;
    public GameObject middle;

    // Initie la sauvegarde la position des premiers tuyaux
    private Vector3 pipe1UpOriginalTransform;
    private Vector3 pipe1DownOriginalTransform;
    private Vector3 middleOriginalTransform;

    // Coordonées Y des tuyaux du haut et du bas. L'autre sert à calculer la différence 
    private float upY;
    private float downY;
    private float difUpDown;

    void Start()
    {
        // Sauvegardre les positions initiales
        pipe1UpOriginalTransform = pipeUp.transform.position;
        pipe1DownOriginalTransform = pipeDown.transform.position;
        middleOriginalTransform = middle.transform.position;

        // Calcule l'espace entre les deux tuyaux
        difUpDown = pipe1UpOriginalTransform.y - pipe1DownOriginalTransform.y;
    }

    void Update()
    { 
        // Fait bouger les tuyaux
        pipeUp.GetComponent<Rigidbody2D>().velocity = gameManager.pipesSpeed;
        pipeDown.GetComponent<Rigidbody2D>().velocity = gameManager.pipesSpeed;
        middle.GetComponent<Rigidbody2D>().velocity = gameManager.pipesSpeed;

        // Les fait respawn si ils sortent
        if ((pipeUp.transform.position.x < gameManager.leftCameraBorder.x - (pipeUp.GetComponent<SpriteRenderer>().bounds.size.x/2)) && (GameObject.FindGameObjectsWithTag("boss").Length == 0))
        {
            upY = Random.Range(4.5f, 7f);
            downY = upY - difUpDown - 0.2f;
            pipeUp.transform.position = new Vector3 (15f, upY, pipe1UpOriginalTransform.z);
            pipeDown.transform.position = new Vector3(15f, downY, pipe1DownOriginalTransform.z);
            middle.transform.position = new Vector3(15f, (upY+downY)/2, middleOriginalTransform.z);
        }
        // Si un tuyaux sort, détruit tout les tuyaux
        else if ((pipeUp.transform.position.x < gameManager.leftCameraBorder.x - (pipeUp.GetComponent<SpriteRenderer>().bounds.size.x / 2)) && (GameObject.FindGameObjectsWithTag("boss").Length == 1))
        {
            foreach (GameObject pairedPipes in GameObject.FindGameObjectsWithTag("pairedPipes"))
            {
                Destroy(pairedPipes);
            }
        }
    }
}
