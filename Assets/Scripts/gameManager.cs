using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class gameManager : MonoBehaviour
{
    //Bordures de caméra
    public static Vector2 leftCameraBorder;
    public static Vector2 rightCameraBorder;
    public static Vector2 upCameraBorder;
    public static Vector2 downCameraBorder;

    // Initie la variable pour le nom de la scène
    private string currentSceneName;

    // Initie la variable contenant le gameObject du joueur
    public static GameObject player;
    public static int attackDamage;
    private float spawnInterval = 1.25f;
    public static bool canAttack = false;

    // Initie les variables liées au score
    public static int score;
    public static bool isFinished;

    // Initie les variables liées au déblacement des entités
    public static Vector2 pipesSpeed = new(-3, 0);
    public static Vector2 backgroundSpeed = new (-1, 0);
    public static float acceleration;

    // Initie la variable pour le nombre de clonage (NB: Utilisée pour les tuyaux (pipes) et les images de fond (background))
    private int numberOfClone;
    private int numberOfMaxItteration = 6;

    // Les gameobjects Original seront ceux à cloner et les Clones sont tous ceux rajoutés
    // Initie les variables pour la création et le placement des tuyaux
    private GameObject pipesOriginal;
    private GameObject pipesClone;
    private float xPipesGap = 6f;
    private float yPipesPosition;

    // Initie les variables pour la création et le placement des images de fonds
    private GameObject backgroundOriginal;
    private GameObject backgroundClone;
    private float xBackgroundGap = 5.33f;
    public static float positionRestartX;

    // Initie les variables pour les ennemis
    private int tirageSpawn;
    private GameObject billOriginal;
    private GameObject redFlyingKoopaOriginal;
    private GameObject blueFlyingKoopaOriginal;
    private GameObject bowserOriginal;

    void Start()
    {
        // Récupère tout les bords de l'écran
        leftCameraBorder = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, 0));
        rightCameraBorder = Camera.main.ViewportToWorldPoint(new Vector3(1, 0, 0));
        upCameraBorder = Camera.main.ViewportToWorldPoint(new Vector3(0, 1, 0));
        downCameraBorder = Camera.main.ViewportToWorldPoint(new Vector3(1, 0, 0));

        // Récupère l'objet du joueur
        player = GameObject.FindGameObjectWithTag("wazo");

        // Récupère le nom de la scène actuelle
        currentSceneName = SceneManager.GetActiveScene().name;

        // Assigne le score et l'accélération de base. Indique aussi que la partie n'est pas finie
        score = 0;
        acceleration = -3f;
        isFinished = false;

        // Récupère le background de base et génère les autres afin de les faire apparaitre pour recouvrir le fond
        backgroundOriginal = GameObject.FindGameObjectWithTag("backgrounds");
        positionRestartX = (xBackgroundGap * numberOfMaxItteration)/2;
        generateBackgrounds();

        // Gère les attaques
        InvokeRepeating("BirdAttacking", 0f, spawnInterval);

        // Vérifie la scène afin d'éviter de générer des erreurs
        if (currentSceneName == "scene2-Menu")
        {
            backgroundSpeed = new Vector2(-1, 0); // Correction d'un bug qui faisait que des fois le background de l'accueil ne bougeait pas
        }
        else if (currentSceneName == "scene3-Game")
        {
            // Génère la première paire de tuyau et la place
            pipesOriginal = Resources.Load("gameObjects/pairPipe") as GameObject; 
            pipesOriginal.transform.position = new Vector3(3, 0, 0);

            // Génère les ennemis afin de pouvoir les appeler plus tard
            billOriginal = Resources.Load("gameObjects/bill") as GameObject; 
            redFlyingKoopaOriginal = Resources.Load("gameObjects/redFlyingKoopa") as GameObject;
            blueFlyingKoopaOriginal = Resources.Load("gameObjects/blueFlyingKoopa") as GameObject;
            bowserOriginal = Resources.Load("gameObjects/Bowser") as GameObject;
        }
    }

    void Update()
    {
        if (currentSceneName == "scene3-Game")
        {
            // Génère les paires de tuyaux si elles ne le sont pas déjà mais ne le fait pas si un boss est présent
            if ((GameObject.FindGameObjectsWithTag("pairedPipes").Length < 3) && (GameObject.FindGameObjectsWithTag("boss").Length == 0))
            {
                generatePipes();
            }

            // Affiche le score
            GameObject.FindGameObjectWithTag("score").GetComponent<scoreController>().UpdateScore(score);

            // Si la partie est finie, arrête le fond, sinon l'accélère (calcul au score)
            if (isFinished == true) 
            {
                pipesSpeed = new Vector2(0, 0);
                backgroundSpeed = new Vector2(0, 0);
            }
            else
            {
                pipesSpeed = new Vector2(acceleration, 0);
                backgroundSpeed = new Vector2(acceleration / 3, 0);
            }
        }
    }

    // Classes servant à créer mes gameObjects
    // Génère les backgrounds
    private void generateBackgrounds()
    {
        numberOfClone = 1;
        while (numberOfClone != numberOfMaxItteration)
        {
            backgroundClone = Instantiate(backgroundOriginal);
            backgroundClone.transform.position = new Vector3(backgroundOriginal.transform.position.x + (xBackgroundGap * numberOfClone), backgroundOriginal.transform.position.y);
            backgroundClone.transform.parent = backgroundOriginal.transform.parent;
            numberOfClone += 1;
        }
    }

    //Génère les paires de tuyaux
    private void generatePipes()
    {
        numberOfClone = 1;
        pipesClone = Instantiate(pipesOriginal);
        pipesClone.transform.parent = GameObject.Find("03 - Pipe").transform;
        while (numberOfClone != 4)
        {
            yPipesPosition = Random.Range(-3.01f, 3.01f);
            pipesClone = Instantiate(pipesOriginal);
            pipesClone.transform.position = new Vector3(pipesOriginal.transform.position.x + (xPipesGap * numberOfClone), yPipesPosition);
            pipesClone.transform.parent = GameObject.Find("03 - Pipe").transform;

            //Changer la couleur (tests)
            //pipesClone.transform.GetChild(0).GetComponent<SpriteRenderer>().sprite = Resources.Load("Textures/downPipeBronze") as Sprite;

            numberOfClone += 1;
        }
    }

    // Génère aléatoirement les ennmis
    public void generateEnemy()
    {
        tirageSpawn = Random.Range(1,100);
        if (tirageSpawn >= 1 && tirageSpawn < 25)
        {
            billOriginal.transform.position = new Vector3(11, Random.Range(-5f, 5f));
            billOriginal.GetComponent<enemyManager>().speed = new Vector2(acceleration - 1f, 0);
            Instantiate(billOriginal);
        }
        else if (tirageSpawn >= 25 && tirageSpawn < 40)
        {
            redFlyingKoopaOriginal.transform.position = new Vector3(11, Random.Range(-5f, 5f));
            redFlyingKoopaOriginal.GetComponent<enemyManager>().speed = new Vector2(acceleration - 1f, 0);
            Instantiate(redFlyingKoopaOriginal);
        }
        else if (tirageSpawn >= 40 && tirageSpawn < 55)
        {
            blueFlyingKoopaOriginal.transform.position = new Vector3(11, Random.Range(-5f, 5f));
            blueFlyingKoopaOriginal.GetComponent<enemyManager>().speed = new Vector2(acceleration - 1.5f, 0);
            Instantiate(blueFlyingKoopaOriginal);
        }
        else if (tirageSpawn >= 55 && tirageSpawn <= 60 && GameObject.FindGameObjectsWithTag("boss").Length == 0)
        {
            canAttack = true;
            bowserOriginal.transform.position = new Vector3(8f, -7);
            Instantiate(bowserOriginal);
        }
        
    }

    // Attaque si possible
    void BirdAttacking()
    {
        if (canAttack == true)
        {
            Instantiate(Resources.Load("gameObjects/birdShot") as GameObject, player.transform.position, transform.rotation); // Crée une nouvelle instance de "gameObjectToSpawn"
        }
        
    }
}
