using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class music : MonoBehaviour
{
    // Initie l'instance de la musique
    public static music instance;

    // Initie les deux musiques voulues
    public AudioClip musicClip1;
    public AudioClip musicClip2;

    private AudioSource audioSource;

    private void Awake()
    {
        // S'il n'y a pas d'instance, en créer une et lui donne une protection au changement de scène
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject); // La dites protection
        }
        // Sinon détruit l'objet afin d'éviter que plusieurs musiques ce jouent 
        else
        {
            Destroy(gameObject);
        }
    }

    private void Start()
    {
        // Charge et joue la première musique (mainTheme)
        audioSource = GetComponent<AudioSource>();
        audioSource.clip = musicClip1;
        audioSource.Play();
    }

    // Permet de changer la musique
    public void ChangeMusic()
    {
        if (audioSource.clip == musicClip1)
        {
            audioSource.clip = musicClip2;
        }
        else if (audioSource.clip == musicClip2 || gameManager.isFinished == true)
        {
            audioSource.clip = musicClip1;
        }

        audioSource.Play();
    }
}
