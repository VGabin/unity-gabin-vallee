﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;
using UnityEngine.Networking;

public class clickButton : MonoBehaviour
{
    // Initie le nom de la scène actuel
    private string currentSceneName;

    // Si bouton cliqué
    public void onClick()
    {
        // Charge le nom de la scene
        currentSceneName = SceneManager.GetActiveScene().name;

        if (gameObject.name == "buttonTwitter")
        {
            string tweetText = "Bonjour Twitter ! Je suis en train de jouer à un #FapLeWazo et j'ai fait un super score de " + gameManager.score.ToString() + " !";
            string url = "https://twitter.com/intent/tweet?text=" + UnityWebRequest.EscapeURL(tweetText);
            Application.OpenURL(url);
        }
        else if (gameObject.name == "buttonLinkdlIn")
        {
            string url = "https://www.linkedin.com/in/gabin-vallée-89440119a/";
            Application.OpenURL(url);
        }
        else
        {
            // En fonction de la scene acutel, change la scene
            if (currentSceneName == "scene2-Menu")
            {
                SceneManager.LoadScene("scene3-Game");
            }
            else if (currentSceneName == "scene3-Game")
            {
                SceneManager.LoadScene("scene2-Menu");
            }
        }
    }
}
