using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class endAction : MonoBehaviour
{
    // Initie le chargement de la musique
    private music musicScript;

    void Start()
    {
        // Signe la fin pour arrêter les Tuyaux et le background
        gameManager.isFinished = true;
        
        // Détruit les composants qui permettent au joueur de jouer
        Destroy(GetComponent<touchAction>());
        Destroy(GetComponent<collideManagerBird>());

        if (GameObject.FindGameObjectsWithTag("boss").Length != 0)
        {
            // Désactive l'attaque de l'wazo
            gameManager.canAttack = false;

            // Détruit les attaques si l'wazo était contre un boss
            foreach (GameObject birdAttack in GameObject.FindGameObjectsWithTag("birdAttack"))
            {
                Destroy(birdAttack);
            }
            
            // Change la musique si la musique du boss est en cours
            musicScript = music.instance;
            musicScript.ChangeMusic();
        }

        // Charge et joue la musique de game over
        AudioSource audioDeath  = gameObject.AddComponent<AudioSource>();
        audioDeath.clip = Resources.Load("Sounds/DeathSound") as AudioClip;
        audioDeath.Play();
    }

    void Update()
    {
        // Fait tourner l'wazo
        gameObject.transform.Rotate(new Vector3(0, 0, -2));
    }
}
