using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class birdAttack : MonoBehaviour
{
    void Update()
    {
        gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(-(gameManager.acceleration) + 1f, 0);
        if (gameObject.transform.position.x > gameManager.rightCameraBorder.x - (gameObject.GetComponent<SpriteRenderer>().bounds.size.x / 2))
        {
            Destroy(gameObject);
        }
    }
}
