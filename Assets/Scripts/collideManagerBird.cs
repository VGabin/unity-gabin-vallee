using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class collideManagerBird : MonoBehaviour
{
    // Initie l'audio de game over
    private AudioSource audioPoint;

    // Initie de gameObject contenant le canva avec le texte et le bouton
    private GameObject gameOver;

    void Start()
    {
        // Charge le son de quand on passe en deux tuyaux
        audioPoint = gameObject.AddComponent<AudioSource>();
        audioPoint.clip = Resources.Load("Sounds/PointSound") as AudioClip;

        // Désactive gameObject Game Over
        gameOver = GameObject.FindGameObjectWithTag("gameOver");
        gameOver.SetActive(false);
    }

    void Update()
    {
        // Gère la collision avec le hat et le bas de l'écran : si on touche le haut ça nous téléporte en bas et inversement
        if (gameObject.transform.position.y < gameManager.downCameraBorder.y)
        {
            gameObject.transform.position = new Vector3(gameObject.transform.position.x,4.8f);
        }
        else if (gameObject.transform.position.y > gameManager.upCameraBorder.y)
        {
            gameObject.transform.position = new Vector3(gameObject.transform.position.x, -4.8f);
        }
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        // Si touche un ennemi ou une attaque : perds
        if ((col.gameObject.tag == "pipes") || (col.gameObject.tag == "enemy") || (col.gameObject.tag == "bossAttack"))
        {
            gameOver.SetActive(true);
            gameObject.AddComponent<endAction>();
        }
        // Si passe entre deux tuyaux : gagne un point
        if (col.gameObject.tag == "pointsUp")
        {
            gameManager.score += 1;
            gameManager.attackDamage = 5 + gameManager.score;
            gameManager.acceleration -= 0.1f;
            GameObject.FindGameObjectWithTag("gameManager").GetComponent<gameManager>().generateEnemy();
            audioPoint.Play();
        }

    }
}
